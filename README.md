# piperci-demo

## Requirements
```
Vagrant >= 2.2.7
ansible >= 2.7
```

## Install PiperCI

```
pip install ansible==2.7
cd dev/
vagrant up --provider virtualbox
cat ../install-info.md
```

## Deploy flake8 FaaS

```
vagrant ssh
sudo su - pidev
git clone https://gitlab.com/dreamer-labs/piperci/piperci-flake8-faas.git
cd piperci-flake8-faas
faas-cli template pull https://gitlab.com/dreamer-labs/piperci/piperci-faas-templates
# faas build
faas deploy
cd ~/
git clone https://gitlab.com/dreamer-labs/piperci/piperci-demo.git
cd piperci-demo/
edit piperci.d/default/stages.yml
```

## Example project piperci.d/stages.yml

```
---
stages:
  - name: style
    deps:
    tasks:
      - name: flake8
        uri: /function/piperci-flake8
        config:
          lint_paths:
            - piperci/
```
