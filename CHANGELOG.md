# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [1.0.2](https://gitlab.com/ragingpastry/piperci-demo/compare/v1.0.1...v1.0.2) (2019-07-31)

## [1.0.1](https://gitlab.com/ragingpastry/piperci-demo/compare/v1.0.0...v1.0.1) (2019-07-31)


### Bug Fixes

* add changelog to git ([a3908a5](https://gitlab.com/ragingpastry/piperci-demo/commit/a3908a5))
